package com.sc.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.sc.db.MarkPoint;
import com.sc.lane.R;
import com.sc.map.SnapshotDoneListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class ViewRecordActivity extends Activity{
	private Context mContext;
	private MapView mMapView;
	private BaiduMap mMap;
	private BitmapDescriptor mMarkStarting;
	private BitmapDescriptor mMarkMiddle;
	private BitmapDescriptor mMarkEnd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		SDKInitializer.initialize(this);
		setContentView(R.layout.view_record_map);
        // 获取地图控件引用
 		mMapView = (MapView)this.findViewById(R.id.bmapView);
 		mMap = mMapView.getMap();
 		mMarkStarting = BitmapDescriptorFactory.fromResource(R.drawable.start);
 		mMarkMiddle = BitmapDescriptorFactory.fromResource(R.drawable.middle);
 		mMarkEnd = BitmapDescriptorFactory.fromResource(R.drawable.end);
 		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(14.0f);
 		mMap.setMapStatus(msu);
		
		this.mContext = this;
		
		Intent intent = this.getIntent();
		List<MarkPoint> points = (List<MarkPoint>) intent.getSerializableExtra("points");
		for(MarkPoint point : points){
			Log.i("info", point.toString());
		}
		
		createOverlay(points);
		drawLine(points);
	}
	
	private void drawLine(List<MarkPoint> points) {
		if(points != null && points.size() > 1)
		{
			List<LatLng> vertexs = new ArrayList<LatLng>();
			for(MarkPoint point: points)
			{
				vertexs.add(new LatLng(point.getLatitude(), point.getLongitude()));
			}
			
			OverlayOptions ooPolyline = new PolylineOptions().width(3)
					.color(0xAAFF0000).points(vertexs);
			mMap.addOverlay(ooPolyline);
		}
	}

	public void createOverlay(List<MarkPoint> points) { 
		int starting = 0;
		int ending = 0;
		
		if(points != null){
			if(points.size() > 1){
				ending = points.size() - 1;
			}
			
			/* 显示起点 */
			LatLng sourceLatLng = new LatLng(points.get(starting).getLatitude(), 
					points.get(starting).getLongitude());
			mMap.addOverlay(new MarkerOptions().position(sourceLatLng).icon(mMarkStarting)
					.zIndex(9).draggable(false));
			
			/* 显示终点 */
			sourceLatLng = new LatLng(points.get(ending).getLatitude(), 
					points.get(ending).getLongitude());
			mMap.addOverlay(new MarkerOptions().position(sourceLatLng).icon(mMarkEnd)
					.zIndex(9).draggable(false));
			
			/* 显示中间点 */
			for(int i = 1; i < ending; ++i){
				sourceLatLng = new LatLng(points.get(i).getLatitude(), 
						points.get(i).getLongitude());
				mMap.addOverlay(new MarkerOptions().position(sourceLatLng).icon(mMarkMiddle)
						.zIndex(9).draggable(false));
			}
			
			/* 调整地图到指定的经纬度 */
			MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(sourceLatLng);
	 		mMap.setMapStatus(msu);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		menu.add(this.getResources().getString(R.string.share));
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		mMap.snapshot(new SnapshotDoneListener(mContext));
		return true;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
		mMapView.onDestroy();
		mMarkStarting.recycle();
		mMarkMiddle.recycle();
		mMarkEnd.recycle();
	}

	@Override
	public void onResume() {
		super.onResume();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();
	}
}
